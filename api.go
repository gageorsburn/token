package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

var (
	tokenMutex   sync.Mutex
	randomTokens map[string]string
)

func init() {
	randomTokens = map[string]string{}
}

func verify(w http.ResponseWriter, r *http.Request) {

	credentials := struct {
		Email string `json:"email"`
		Token string `json:"token"`
	}{}

	json.NewDecoder(r.Body).Decode(&credentials)

	var result string

	if value, ok := randomTokens[credentials.Email]; ok {
		if value != "" && value == credentials.Token {
			log.Printf("Successfully authenticated user: %s", credentials.Email)
			result = "true"
		} else {
			log.Printf("Could not authenticate user: %s", credentials.Email)
			result = "false"
		}
	} else {
		result = "false"
	}

	w.Write([]byte(result))
}

func list(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

	}
}

func generate(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		vars := mux.Vars(r)
		log.Println(vars)
	}
}

func revoke(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodDelete {
		vars := mux.Vars(r)
		log.Println(vars)
	}
}

func random(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		profile := ProfileFromSession(r)

		if profile != nil {

			token := uuid.Must(uuid.NewV4()).String()
			token = strings.Replace(token, "-", "", -1)

			w.Write([]byte(token))

			go func() {

				tokenMutex.Lock()
				randomTokens[profile.Email] = token
				tokenMutex.Unlock()

				log.Printf("Generated token for user: %s", profile.Email)

				timer := time.NewTimer(60 * time.Second)
				<-timer.C

				log.Printf("Token expired for user %s", profile.Email)

				tokenMutex.Lock()

				if randomTokens[profile.Email] == token {
					randomTokens[profile.Email] = ""
				}

				tokenMutex.Unlock()
			}()
		}
	}
}
