package main

import (
	"context"
	"encoding/gob"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/gorilla/sessions"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	validator "gopkg.in/go-playground/validator.v9"
)

var (
	//SessionStore ...
	SessionStore sessions.Store
	//OAuthConfig ...
	OAuthConfig *oauth2.Config
)

// Profile ..
type Profile struct {
	ID          string `json:"sub" validate:"required"`
	Domain      string `json:"hd" validate:"required"`
	DisplayName string `json:"name" validate:"required"`
	ImageURL    string `json:"picture" validate:"required,url"`
	Email       string `json:"email" validate:"required,email"`
}

func init() {
	log.Println("init")
	gob.Register(&oauth2.Token{})
	gob.Register(&Profile{})
	cookieStore := sessions.NewCookieStore([]byte("something-very-secret"))
	cookieStore.Options = &sessions.Options{
		HttpOnly: true,
	}
	SessionStore = cookieStore

	oauthClient := os.Getenv("OAUTH2_CLIENT")

	if oauthClient == "" {
		oauthClient = "434483713349-38ghgosuqjpsuss1nl78vqe7snnlidvi.apps.googleusercontent.com"
	}
	oauthSecret := os.Getenv("OAUTH2_SECRET")

	if oauthSecret == "" {
		oauthSecret = "QkKMct5_0VnWxHGLMMeRTlA4"
	}

	OAuthConfig = configureOAuthClient(oauthClient, oauthSecret)
}

func configureOAuthClient(clientID, clientSecret string) *oauth2.Config {
	redirectURL := os.Getenv("OAUTH2_CALLBACK")
	if redirectURL == "" {
		redirectURL = "http://token.lab.osi.io/callback"
	}

	return &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}
}

// ProfileFromSession ..
func ProfileFromSession(r *http.Request) *Profile {
	// log.Print("ProfileFromSession()")
	// log.Printf("Header: %s", r.Header)
	session, err := SessionStore.Get(r, "default")
	if err != nil {
		log.Print(err)
		return nil
	}

	// log.Print("ProfileFromSession() setting session token")
	tok, ok := session.Values["oauth_token"].(*oauth2.Token)
	if !ok || !tok.Valid() {
		return nil
	}
	profile, ok := session.Values["google_profile"].(*Profile)
	if !ok {
		return nil
	}
	return profile
}

func validateRedirectURL(path string) (string, error) {
	if path == "" {
		return "/", nil
	}

	parsedURL, err := url.Parse(path)
	if err != nil {
		return "/", err
	}
	if parsedURL.IsAbs() {
		return "/", errors.New("URL must not be absolute")
	}
	return path, nil
}

func callback(w http.ResponseWriter, r *http.Request) {
	oauthFlowSession, err := SessionStore.Get(r, r.FormValue("state"))
	if err != nil {
		fmt.Println(err, "invalid state parameter. try logging in again.")
	}

	redirectURL, ok := oauthFlowSession.Values["redirect"].(string)
	// Validate this callback request came from the app.
	if !ok {
		fmt.Println(err, "invalid state parameter. try logging in again.")
	}

	code := r.FormValue("code")
	tok, err := OAuthConfig.Exchange(context.Background(), code)
	if err != nil {
		fmt.Println(err, "could not get auth token: %v", err)
	}

	session, err := SessionStore.New(r, "default")
	if err != nil {
		fmt.Println(err, "could not get default session: %v", err)
	}

	ctx := context.Background()
	profile, err := fetchProfile(ctx, tok)
	if err != nil {
		fmt.Println(err, "could not fetch Google profile: %v", err)
	}

	osiEmail := profile.Domain == "osi.io"

	if osiEmail {
		session.Values["oauth_token"] = tok
		session.Values["google_profile"] = profile
		if err := session.Save(r, w); err != nil {
			fmt.Println(err, "could not save session: %v", err)
			http.Redirect(w, r, redirectURL, http.StatusInternalServerError)
		} else {
			http.Redirect(w, r, redirectURL, http.StatusFound)
		}
	} else {
		http.Redirect(w, r, redirectURL, http.StatusUnauthorized)
	}
}

func fetchProfile(ctx context.Context, tok *oauth2.Token) (*Profile, error) {
	client := oauth2.NewClient(ctx, OAuthConfig.TokenSource(ctx, tok))
	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	prof, err := parseProfile(data)
	return &prof, err
}

func parseProfile(data []byte) (prof Profile, err error) {
	err = json.Unmarshal([]byte(data), &prof)
	if err != nil {
		return
	}
	// Resize the image
	prof.ImageURL = prof.ImageURL + "?sz=50"
	validate := validator.New()
	err = validate.Struct(&prof)
	return
}
func login(w http.ResponseWriter, r *http.Request) {
	sessionID := uuid.Must(uuid.NewV4()).String()
	oauthFlowSession, _ := SessionStore.New(r, sessionID)
	oauthFlowSession.Options.MaxAge = 10 * 60 // 10 minutes
	redirectURL, _ := validateRedirectURL(r.FormValue("redirect"))
	oauthFlowSession.Values["redirect"] = redirectURL

	if err := oauthFlowSession.Save(r, w); err != nil {
		fmt.Println(err)
	}

	url := OAuthConfig.AuthCodeURL(sessionID, oauth2.ApprovalForce,
		oauth2.AccessTypeOnline)
	http.Redirect(w, r, url, http.StatusFound)
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, err := SessionStore.New(r, "default")
	if err != nil {
		fmt.Println(err, "could not get default session: %v", err)
	}
	session.Options.MaxAge = -1 // Clear session.
	if err := session.Save(r, w); err != nil {
		fmt.Println(err, "could not save session: %v", err)
	}
	redirectURL := r.FormValue("redirect")
	if redirectURL == "" {
		redirectURL = "/"
	}
	http.Redirect(w, r, redirectURL, http.StatusFound)
}
