package main

import (
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func home(w http.ResponseWriter, r *http.Request) {

	profile := ProfileFromSession(r)
	log.Printf("%+v", profile)
	data := struct {
		Profile *Profile
	}{
		profile,
	}

	tmpl, err := template.ParseFiles("/templates/index.html")

	if err != nil {
		log.Println(err)
	}

	tmpl.Execute(w, data)
}

func main() {
	router := mux.NewRouter()

	// api
	router.HandleFunc("/api/token/verify", verify)
	router.HandleFunc("/api/token/list", list)
	router.HandleFunc("/api/token/generate", generate)
	router.HandleFunc("/api/token/revoke", revoke)
	router.HandleFunc("/api/token/random", random)

	//authentication
	router.HandleFunc("/callback", callback)
	router.HandleFunc("/login", login)
	router.HandleFunc("/logout", logout)

	// spa
	router.HandleFunc("/", home)

	http.ListenAndServe(":80", router)
}
