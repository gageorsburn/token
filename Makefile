build:
	docker build -t onesourceintegrations/token:latest .

push:
	docker push onesourceintegrations/token:latest

deploy:
	kubectl apply -f deploy.yml -n vpn