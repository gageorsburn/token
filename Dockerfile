FROM golang:1.11.5 as builder
ENV GO111MODULE=on
ADD . /app
WORKDIR /app
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o token *.go

FROM golang:alpine as production
RUN apk add --no-cache ca-certificates openssl
ADD /templates/index.html /templates/index.html
COPY --from=builder /app/token /
CMD ["/token"]