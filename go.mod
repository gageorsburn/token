module token

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/gorilla/mux v1.7.0
	github.com/gorilla/sessions v1.1.3
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0
	gopkg.in/go-playground/validator.v9 v9.26.0
)
